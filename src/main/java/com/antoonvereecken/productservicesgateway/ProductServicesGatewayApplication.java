package com.antoonvereecken.productservicesgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductServicesGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductServicesGatewayApplication.class, args);
	}

}
