package com.antoonvereecken.productservicesgateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile({"localhost", "!local-discovery"})
public class LocalHostRouteConfig {

    @Bean
    public RouteLocator localHostRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(
                        r -> r.path("/api/v1/product*", "/api/v1/product/*", "/api/v1/product/ean/*")
                                .uri("http://localhost:8085")
                                .id("product-service")
                )
                .route(
                        r -> r.path("/api/v1/customer*", "/api/v1/customer/**")
                                .uri("http://localhost:8087")
                                .id("product-order-service")
                )
                .route(
                        r -> r.path("/api/v1/product/*/inventory")
                                .uri("http://localhost:8086")
                                .id("product-inventory-service")
                )
                .build();
    }
}
